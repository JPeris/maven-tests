package net.javierperis.maven.services;

import org.springframework.stereotype.Service;

@Service("myServiceOne")
public class MyServiceOne {

    public String getA() {
        return "A";
    }

}
