package net.javierperis.maven.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration("classpath:context.xml")
public class MyServiceOneTest extends
        AbstractJUnit4SpringContextTests {

    @Autowired
    private MyServiceOne myServiceOne;

    @Test
    public void shouldGetA() {
        // .. test body here
    }
}
