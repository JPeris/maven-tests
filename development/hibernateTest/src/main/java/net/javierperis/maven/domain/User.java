package net.javierperis.maven.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class User {
    private Long id;
    private String username;
    private String password;
    public void setUsername(String username) {
	this.username = username;
    }

    @Column
    public String getUsername() {
	return username;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    @Column
    public String getPassword() {
	return password;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Id
    @GeneratedValue
    public Long getId() {
	return id;
    }

}
