package net.javierperis.maven.app;

import net.javierperis.maven.domain.User;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;

public class App
{
    public static void main( String[] args ) {
	AnnotationConfiguration configuration = new
	    AnnotationConfiguration()
	    .addPackage("net.javierperis.maven.domain")
	    .addAnnotatedClass(User.class);
	Session session = configuration.buildSessionFactory().
	    openSession();
	User user1 = new User();
	user1.setUsername("hello");
	user1.setPassword("world");
	session.save(user1);
	session.close();
    }
}
